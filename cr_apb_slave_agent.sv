/**
 * @author: https://gitee.com/chenran9695
 * */
import uvm_pkg::*;
`include "uvm_macros.svh"

typedef class apb_sq_item;

class apb_slave_drv extends uvm_driver #(apb_sq_item);
	`uvm_component_utils(apb_slave_drv)
	
	virtual apb_if vif;
	bit[31:0] mem [bit[31:0]];

	function new(string name = "apb_slave_drv", uvm_component parent);
		super.new(name, parent);
	endfunction : new

	virtual task run_phase(uvm_phase phase);
		super.run_phase(phase);
		fork
			do_recieve_and_drive();
			do_reset();
		join_none
	endtask : run_phase

	task do_reset();
		`uvm_info(get_type_name(), "starting reset listener", 0)
		forever begin
			@(negedge vif.rstn);
			vif.clk_slv.prdata <= 0;
			mem.delete();
		end
	endtask : do_reset

	task do_recieve_and_drive();
		`uvm_info(get_type_name(), "starting drive listener", 0)
		forever begin
			@(vif.clk_slv);
			if(vif.clk_slv.penable === 1'b0 && vif.clk_slv.psel === 1'b1) begin // 注意：由于clocking的特性，这里已经是在第二周期
				case(vif.clk_slv.pwrite)
					1'b0: do_read();
					1'b1: do_write();
					default: `uvm_fatal(get_type_name(), "invalid pwrite")
				endcase
			end else begin
				do_idle();
			end
		end
	endtask : do_recieve_and_drive

	task do_write();
		bit [31:0] data;
		bit [31:0] addr;
		addr = vif.clk_slv.paddr;
		data = vif.clk_slv.pwdata;
		mem[addr] = data;
	endtask : do_write

	
	task do_read();
		bit [31:0] data;
		bit [31:0] addr;
		addr = vif.clk_slv.paddr;
		if(mem.exists(addr))
			data = mem[addr];
		else
			data = 32'hFFFF_FFFF;
		vif.clk_slv.prdata <= data;
	endtask : do_read

	task do_idle();
		`uvm_info(get_type_name(), "do idle", UVM_HIGH)
		vif.clk_slv.prdata <= 0;
	endtask : do_idle

endclass

class apb_slave_sqr extends uvm_sequencer #(apb_sq_item);
	`uvm_component_utils(apb_slave_sqr)

	function new(string name = "apb_master_sqr", uvm_component parent);
		super.new(name, parent);
	endfunction : new
endclass

class apb_slave_mon extends uvm_monitor;
	`uvm_component_utils(apb_slave_mon)
	uvm_analysis_port#(.T(apb_sq_item)) mon_port; // 二次开发通过tlm方式连接到该端口，从而获取数据
	virtual apb_if vif;

	function new(string name = "apb_slave_mon", uvm_component parent);
		super.new(name, parent);
	endfunction : new

	virtual task run_phase(uvm_phase phase);
		super.run_phase(phase);
		fork
			do_monitor();
		join_none
	endtask : run_phase

	task do_monitor();
		`uvm_info(get_type_name(), "starting monitor listener", 0)
		forever begin
			@(vif.clk_mon);
			if(vif.clk_mon.psel === 1'b1 && vif.clk_mon.penable === 1'b0) begin
				case(vif.clk_mon.pwrite)
					1'b0: mon_read_write(READ);
					1'b1: mon_read_write(WRITE);
					default: `uvm_error(get_type_name(), "ERROR pwrite signal value")
				endcase
			end
		end
	endtask : do_monitor

	task mon_read_write(apb_trans_type trans_type);
		apb_sq_item t = new();
		t.addr = vif.clk_mon.paddr;
		t.data = vif.clk_mon.prdata;
		t.trans_type = trans_type;
		mon_port.write(t);
	endtask
endclass

class apb_slave_agent extends uvm_agent;
	`uvm_component_utils(apb_slave_agent)

	virtual apb_if vif;

	apb_slave_drv drv;
	apb_slave_mon mon;
	apb_slave_sqr sqr;

	function new(string name = "apb_slave_agent", uvm_component parent);
		super.new(name, parent);
	endfunction : new

	virtual function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		drv = apb_slave_drv::type_id::create("drv", this);
		mon = apb_slave_mon::type_id::create("mon", this);
		sqr = apb_slave_sqr::type_id::create("sqr", this);

		if(!uvm_config_db#(virtual apb_if)::get(this, "", "vif", vif))
			`uvm_fatal(get_type_name(), "vif init failed")
	endfunction : build_phase

	virtual function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);
		drv.seq_item_port.connect(sqr.seq_item_export);
		drv.vif = this.vif;
		mon.vif = this.vif;
	endfunction : connect_phase
endclass