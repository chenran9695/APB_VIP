/**
 * @author: https://gitee.com/chenran9695
 * */
class apb_master_drv extends uvm_driver #(apb_sq_item);
	`uvm_component_utils(apb_master_drv)
	virtual apb_if vif;

	function new(string name = "apb_master_drv", uvm_component parent);
		super.new(name, parent);
	endfunction : new

	virtual task run_phase(uvm_phase phase);
		super.run_phase(phase);
		fork
			do_recieve_and_drive();
			do_reset();
		join_none
	endtask : run_phase

	task do_reset();
		`uvm_info(get_type_name(), "starting reset listener", 0)
		forever begin
			@(negedge vif.rstn) begin
				vif.clk_mst.paddr <= 0;
				vif.clk_mst.penable <=0;
				vif.clk_mst.pwrite <= 0;
				vif.clk_mst.pwdata <= 0;
				vif.clk_mst.psel <= 0;
			end
		end
	endtask : do_reset

	task do_recieve_and_drive();
		`uvm_info(get_type_name(), "starting drive listener", 0)
		forever begin
			seq_item_port.get_next_item(req);
			do_drive();
			void'($cast(rsp, req.clone()));
			rsp.set_sequence_id(req.get_sequence_id());
			seq_item_port.item_done(rsp);
			`uvm_info(get_type_name(), "recieve a item from sqr", 0)
		end
	endtask : do_recieve_and_drive

	task do_drive();
		case(req.trans_type)
			IDLE: do_idle();
			WRITE: do_write();
			READ: do_read();
			default : `uvm_fatal(get_type_name(), "error master item")
		endcase
	endtask : do_drive

	task do_read();
		@(vif.clk_mst);
		vif.clk_mst.psel <= 1;
		vif.clk_mst.penable <= 0;
		vif.clk_mst.paddr <= req.addr;
		vif.clk_mst.pwrite <= 0;
		@(vif.clk_mst);
		vif.clk_mst.penable <= 1;
		#100ps req.data = vif.prdata;
		repeat (req.idle_cycles) do_idle();
	endtask : do_read

	task do_write();
		@(vif.clk_mst);
		vif.clk_mst.psel <= 1;
		vif.clk_mst.penable <= 0;
		vif.clk_mst.paddr <= req.addr;
		vif.clk_mst.pwrite <= 1;
		vif.clk_mst.pwdata <= req.data;
		@(vif.clk_mst);
		vif.clk_mst.penable <= 1;
		repeat (req.idle_cycles) do_idle();
	endtask : do_write

	task do_idle();
		@(vif.clk_mst);
		vif.clk_mst.paddr <= 0;
		vif.clk_mst.psel <= 0;
		vif.clk_mst.penable <= 0;
		vif.clk_mst.pwdata <= 0;
	endtask : do_idle
endclass

class apb_master_sqr extends uvm_sequencer #(apb_sq_item);
	`uvm_component_utils(apb_master_sqr)

	function new(string name = "apb_master_sqr", uvm_component parent);
		super.new(name, parent);
	endfunction : new
endclass

class apb_master_mon extends uvm_monitor;
	`uvm_component_utils(apb_master_mon)

	virtual apb_if vif;
	uvm_analysis_port#(.T(apb_sq_item)) mon_port; // 二次开发通过tlm方式连接到该端口，从而获取数据

	function new(string name = "apb_master_mon", uvm_component parent);
		super.new(name, parent);
		mon_port = new("mon_port", this);
	endfunction : new

	virtual task run_phase(uvm_phase phase);
		super.run_phase(phase);
		fork
			do_monitor();
		join_none
	endtask : run_phase

	task do_monitor();
		`uvm_info(get_type_name(), "starting monitor listener", 0)
		forever begin
			@(vif.clk_mon);
			if(vif.clk_mon.psel === 1'b1 && vif.clk_mon.penable === 1'b0) begin
				case(vif.clk_mon.pwrite)
					1'b0: mon_read_write(READ);
					1'b1: mon_read_write(WRITE);
					default: `uvm_error(get_type_name(), "ERROR pwrite signal value")
				endcase
			end
		end
	endtask : do_monitor

	task mon_read_write(apb_trans_type trans_type);
		apb_sq_item t = new();
		t.addr = vif.clk_mon.paddr;
		t.data = vif.clk_mon.prdata;
		t.trans_type = trans_type;
		mon_port.write(t);
	endtask
endclass

class apb_master_agent extends uvm_agent;
	`uvm_component_utils(apb_master_agent)

	virtual apb_if vif;

	apb_master_drv drv;
	apb_master_mon mon;
	apb_master_sqr sqr;

	function new(string name = "apb_master_agent", uvm_component parent);
		super.new(name, parent);
	endfunction : new

	virtual function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		drv = apb_master_drv::type_id::create("drv", this);
		mon = apb_master_mon::type_id::create("mon", this);
		sqr = apb_master_sqr::type_id::create("sqr", this);

		if(!uvm_config_db#(virtual apb_if)::get(this, "", "vif", vif))
			`uvm_fatal(get_type_name(), "vif init failed")
	endfunction : build_phase

	virtual function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);
		drv.seq_item_port.connect(sqr.seq_item_export);
		drv.vif = this.vif;
		mon.vif = this.vif;
	endfunction : connect_phase
endclass