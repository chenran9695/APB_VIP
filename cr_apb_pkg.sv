/**
 * @author: https://gitee.com/chenran9695
 * */
package cr_apb_pkg;

 import uvm_pkg::*;
`include "uvm_macros.svh"

`include "cr_apb_sq_item.sv"
`include "cr_apb_sq.sv"
`include "cr_apb_master_agent.sv"
`include "cr_apb_slave_agent.sv"

endpackage