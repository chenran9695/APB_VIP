/**
 * @author: https://gitee.com/chenran9695
 * */
import uvm_pkg::*;
`include "uvm_macros.svh"

typedef enum {IDLE, WRITE, READ} apb_trans_type;

class apb_sq_item extends uvm_sequence_item;
	
	rand apb_trans_type trans_type;
	rand bit[31:0] addr;
	rand bit[31:0] data;
	rand int idle_cycles;
	
	constraint cstr {
		soft idle_cycles == 1;
	}
	
	`uvm_object_utils_begin(apb_sq_item)
		`uvm_field_int(addr, UVM_ALL_ON)
		`uvm_field_int(data, UVM_ALL_ON)
		`uvm_field_int(idle_cycles, UVM_ALL_ON)
		`uvm_field_enum(apb_trans_type, trans_type, UVM_ALL_ON)
	`uvm_object_utils_end
	
	function new(string name = "apb_sq_item");
		super.new(name);
	endfunction
	
endclass