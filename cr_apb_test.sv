/**
 * @author: https://gitee.com/chenran9695
 * */
import uvm_pkg::*;
import cr_apb_pkg::*;
`include "uvm_macros.svh"
class apb_env extends uvm_env;
	`uvm_component_utils(apb_env)
	apb_master_agent mst_agt;
	apb_slave_agent slv_agt;
	//TODO add checker

	function new(string name = "apb_env", uvm_component parent);
		super.new(name, parent);
	endfunction

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		mst_agt = apb_master_agent::type_id::create("mst_agt", this);
		slv_agt = apb_slave_agent::type_id::create("slv_agt", this);
	endfunction
endclass

class apb_virtual_sqr extends uvm_sequencer;
	`uvm_component_utils(apb_virtual_sqr)
	apb_master_sqr mst_sqr;

	function new (string name = "apb_virtual_sqr", uvm_component parent);
		super.new(name, parent);
	endfunction
endclass

class apb_virtual_seq extends apb_base_seq;
	bit[31:0] memo[bit[31:0]];
	bit[31:0] addr;
	bit[31:0] data;
	rand int test_cycles = 500;
	constraint crst {
		soft test_cycles == 500;
	}

	`uvm_object_utils(apb_virtual_seq)
	`uvm_declare_p_sequencer(apb_virtual_sqr)

	function new(string name = "apb_virtual_seq");
		super.new(name);
	endfunction : new

	apb_single_write_seq apb_write_seq;
	apb_single_read_seq apb_read_seq;
	apb_single_idle_seq apb_idle_seq;


	task body();

		this.wait_for_reset();
		this.wait_cycles(10);

		`uvm_info(get_type_name(), "starting seq", UVM_LOW);

		// 放飞自我，一把梭哈，全覆盖
		single_write();
		burst_write(2);
		burst_write(4);
		burst_write(8);
		burst_write(16);
		burst_write(32);

		single_read();
		single_read(0);
		single_write();
	endtask : body

	task single_write();
		repeat(test_cycles) begin
			addr = get_rand_32bit();
			data = get_rand_32bit();
			`uvm_info(get_type_name(), $sformatf("start write, addr: 32'h%8x, data: 32'h%8x",addr, data), UVM_LOW)
			`uvm_do_on_with(apb_write_seq, p_sequencer.mst_sqr, {data == local::data; addr == local::addr;})
			memo[addr] = data;
		end
	endtask

	task single_read(int cycles = 1);
		repeat(test_cycles) begin
			addr = get_rand_32bit();
			data = get_rand_32bit();
			`uvm_info(get_type_name(), $sformatf("start read, addr: 32'h%8x, data: 32'h%8x",addr, data), UVM_LOW)
			`uvm_do_on_with(apb_read_seq, p_sequencer.mst_sqr, {addr == local::addr;idle_cycles == cycles;})
			void'(do_check_data(addr, apb_read_seq.data));
			`uvm_info(get_type_name(), "end seq", UVM_LOW);
		end
	endtask

	task burst_read();
		repeat(test_cycles) begin
			addr = get_rand_32bit();
			data = get_rand_32bit();
			`uvm_info(get_type_name(), $sformatf("start read, addr: 32'h%8x, data: 32'h%8x",addr, data), UVM_LOW)
			`uvm_do_on_with(apb_read_seq, p_sequencer.mst_sqr, {addr == local::addr;idle_cycles == 0;})
			void'(do_check_data(addr, apb_read_seq.data));
			`uvm_info(get_type_name(), "end seq", UVM_LOW);
		end
	endtask

	task burst_write(int test_cycles);
		repeat(test_cycles) begin
			addr = get_rand_32bit();
			data = get_rand_32bit();
			`uvm_info(get_type_name(), $sformatf("start write, addr: 32'h%8x, data: 32'h%8x",addr, data), UVM_LOW)
			`uvm_do_on_with(apb_write_seq, p_sequencer.mst_sqr, {data == local::data; addr == local::addr;idle_cycles == 0;})
			memo[addr] = data;
		end
		`uvm_do_on(apb_idle_seq, p_sequencer.mst_sqr)
	endtask

	function bit do_check_data(bit[31:0] addr, bit[31:0] data);
		if(memo.exists(addr)) begin
			if(data != memo[addr]) begin
				`uvm_error("COMPDATA", $sformatf("addr: 32'h%8x, actual data 32'h%8x != excepted 32'h%8x", addr, data, memo[addr]))
				return 0;
			end
			return 1;
		end

		if(data != 32'hFFFF_FFFF) begin
			`uvm_error("COMPDATA", $sformatf("addr: 32'h%8x, actual data 32'h%8x != excepted 32'h%8x", addr, data, 32'hFFFF_FFFF))
			return 0;
		end

		return 1;
	endfunction : do_check_data

	task wait_cycles(int cycles);
		repeat(cycles) @(posedge apb_tb.clk);
	endtask : wait_cycles

	task wait_for_reset();
		@(negedge apb_tb.rstn);
		@(posedge apb_tb.rstn);
		`uvm_info(get_type_name(), "rstn rose", UVM_LOW)
	endtask : wait_for_reset

	function bit[31:0] get_rand_32bit();
		bit[31:0] addr;
		void'(std::randomize(addr) with {addr[31:12] == 0;addr[1:0] == 0;addr !=0;});
		return addr;
	endfunction
endclass


class apb_single_write_test extends uvm_test;
	`uvm_component_utils(apb_single_write_test)

	apb_env env;
	apb_virtual_seq seq;
	apb_virtual_sqr sqr;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		env = apb_env::type_id::create("env", this);
		sqr = apb_virtual_sqr::type_id::create("sqr", this);
	endfunction

	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);
		sqr.mst_sqr = env.mst_agt.sqr;
	endfunction


	task run_phase(uvm_phase phase);
		seq = new();
		phase.raise_objection(this);
		seq.start(sqr);
		phase.phase_done.set_drain_time(this, 10us);
		phase.drop_objection(this);
	endtask
endclass