/**
 * @author: https://gitee.com/chenran9695
 * */
interface apb_if(input clk, input rstn);
	logic [31:0] paddr;
	logic psel;
	logic penable;
	logic pwrite;
	logic [31:0] pwdata;
	logic [31:0] prdata;

	clocking clk_mst @(posedge clk);
		default input #1ps output #1ps;
		output paddr, psel, penable, pwrite, pwdata;
		input prdata;
	endclocking

	clocking clk_slv @(posedge clk);
		default input #1ps output #1ps;
		input paddr, psel, penable, pwrite, pwdata;
		output prdata;
	endclocking

	clocking clk_mon @(posedge clk);
		default input #1ps output #1ps;
		input paddr, psel, penable, pwrite, pwdata, prdata;
	endclocking

	covergroup cg_apb_write @(posedge clk iff rstn);
		pwrite: coverpoint pwrite {
			type_option.weight = 0;
			bins write = {1};
			bins read = {0};
		}
		psel: coverpoint psel {
			type_option.weight = 0;
			bins sel = {1};
			bins unsel = {0};
		}

		cmd: cross pwrite, psel {
			bins cmd_write = binsof (psel.sel) && binsof(pwrite.write);
			bins cmd_read = binsof (psel.sel) && binsof(pwrite.read);
			bins cmd_idle = binsof (psel.unsel);
		}
	endgroup

	covergroup cg_apb_trans_timing_group @(posedge clk iff rstn);
		psel: coverpoint psel {
			bins single = (0 => 1 => 1 => 0);
			bins burst_2 = (0 => 1 [*4] => 0);
			bins burst_4 = (0 => 1 [*8] => 0);
			bins burst_8 = (0 => 1 [*16] => 0);
			bins burst_16 = (0 => 1 [*32] => 0);
			bins burst_32 = (0 => 1 [*64] => 0);
		}
		penable: coverpoint penable {
			bins single = (0 => 1 => 0 [*2:10] => 1);
			bins burst = (0 => 1 => 0 => 1);
		}
	endgroup

	covergroup cg_apb_write_read_order_group @(posedge clk iff (rstn && penable));
		write_read_order: coverpoint pwrite {
			bins write_write = (1 => 1);
			bins write_read = (1 => 0);
			bins read_write = (0 => 1);
			bins read_read = (0 => 0);
		}
	endgroup

	initial begin
		automatic cg_apb_write cg_write = new();
		automatic cg_apb_trans_timing_group cg_trans_timing_group = new();
		automatic cg_apb_write_read_order_group cg_write_read_order_group = new();
	end
	// 地址不能包含x
	property p_addr_no_x;
		@(posedge clk) psel |-> !$isunknown(paddr);
	endproperty
	assert property(p_addr_no_x) else `uvm_error("ASSERT", "p_addr_no_x")
	// psel拉高的下一周期penable拉高
	property p_psel_rose_next_cycle_penable_rise;
		@(posedge clk) $rose(psel) |=> $rose(penable);
	endproperty
	assert property(p_psel_rose_next_cycle_penable_rise) else `uvm_error("ASSERT", "p_psel_rose_next_cycle_penable_rise")
	// penable拉高后下一周期拉低
	property p_penable_rose_next_cycle_fell;
		@(posedge clk) $rose(penable) |=> $fell(penable);
	endproperty
	assert property(p_penable_rose_next_cycle_fell) else `uvm_error("ASSERT", "p_penable_rose_next_cycle_fell")
	// 传递周期内，pwdata保持稳定
	property p_pwdata_stable_during_trans_phase;
		@(posedge clk) ((psel && !penable) ##1 (psel && penable)) |-> $stable(pwdata);
	endproperty
	assert property(p_pwdata_stable_during_trans_phase) else `uvm_error("ASSERT", "p_pwdata_stable_during_trans_phase")
	// 传递周期内，paddr保持稳定
	property p_paddr_stable_during_trans_phase;
		@(posedge clk) ((psel && !penable) ##1 (psel && penable)) |-> $stable(paddr);
	endproperty
	assert property(p_paddr_stable_during_trans_phase) else `uvm_error("ASSERT", "p_paddr_stable_during_trans_phase")
	//TODO 完善断言部分

	initial begin
		fork
			forever begin
				wait(rstn == 0);
				$assertoff();
				wait(rstn == 1);
				$asserton();
			end
		join_none
	end

endinterface