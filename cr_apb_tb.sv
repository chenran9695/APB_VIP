 /**
  * @author: https://gitee.com/chenran9695
  * */
`timescale 1ps/1ps
import uvm_pkg::*;
`include "uvm_macros.svh"
`include "cr_apb_test.sv"
`include "cr_apb_if.sv"

module apb_tb;
	bit clk;
	bit rstn;

	initial begin
		fork
			forever begin
				#5ns clk = !clk;
			end
			begin
				#100ns rstn = 1'b1;
				#100ns rstn = 1'b0;
				#100ns rstn = 1'b1;
			end
		join
	end
	
	apb_if vif(clk, rstn);
	
	initial begin
		uvm_config_db#(virtual apb_if)::set(uvm_root::get(), "uvm_test_top.env.mst_agt", "vif", vif);
		uvm_config_db#(virtual apb_if)::set(uvm_root::get(), "uvm_test_top.env.slv_agt", "vif", vif);
		run_test("apb_single_write_test");
	end
	
endmodule
