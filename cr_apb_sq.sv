/**
 * @author: https://gitee.com/chenran9695
 * */
import uvm_pkg::*;
`include "uvm_macros.svh"

class apb_base_seq extends uvm_sequence #(apb_sq_item);
	`uvm_object_utils(apb_base_seq)

	function new(string name = "uvm_sequence");
		super.new(name);
	endfunction : new

	virtual task body();
		`uvm_info(get_type_name(), "starting seq", 0);
		do_trans();
		`uvm_info(get_type_name(), "end seq", 0);
	endtask : body


	virtual task do_trans();
	endtask : do_trans
endclass

class apb_single_write_seq extends apb_base_seq;
	`uvm_object_utils(apb_single_write_seq)

	rand bit[31:0] addr;
	rand bit[31:0] data;
	rand int idle_cycles;

	constraint cstr {
		soft idle_cycles == 1;
	};

	function new(string name = "uvm_sequence");
		super.new(name);
	endfunction : new

	virtual task do_trans();
		
		`uvm_do_with(req, {trans_type == WRITE; addr == local::addr; data == local::data; idle_cycles == local::idle_cycles;})
		get_response(rsp);
	endtask : do_trans
endclass

class apb_single_read_seq extends apb_base_seq;
	`uvm_object_utils(apb_single_read_seq)

	rand bit[31:0] addr;
	rand bit[31:0] data;
	rand int idle_cycles;

	constraint cstr {
		soft idle_cycles == 1;
	};

	function new(string name = "uvm_sequence");
		super.new(name);
	endfunction : new

	virtual task do_trans();
		`uvm_do_with(req, {trans_type == READ; addr == local::addr;idle_cycles == local::idle_cycles;})
		get_response(rsp);
		`uvm_info("apb_single_read_seq", $sformatf("addr: 32'h%8x, data: 32'h%8x", rsp.addr, rsp.data), UVM_LOW)
		data = rsp.data;
	endtask : do_trans
endclass

class apb_single_idle_seq extends apb_base_seq;
	rand bit[31:0] data;
	`uvm_object_utils(apb_single_idle_seq)


	function new(string name = "uvm_sequence");
		super.new(name);
	endfunction : new

	virtual task do_trans();
		`uvm_do_with(req, {trans_type == IDLE; addr == 0;idle_cycles == 0;})
		get_response(rsp);
		`uvm_info("apb_single_idle_seq", $sformatf("addr: 32'h%8x, data: 32'h%8x", rsp.addr, rsp.data), UVM_LOW)
		data = rsp.data;
	endtask : do_trans
endclass